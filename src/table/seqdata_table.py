#-------------------------------------------------------------------------------
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \description
#-------------------------------------------------------------------------------

class SeqdataTable:

  ## Constructor
  # @param name, str, clade name
  # @param database, reference, reference to corresponding SQlite3 instance
  def __init__(self, name=None, database=None):
    self.name = name
    self.database = database
    self.size = 0
    self.idx = {}
    self.create()

  ## Creating table
  # A virtual function
  def create(self):
    raise NotImplementedError("Implement create() method")

  def count_rows(self):
    stmt = "SELECT COUNT(id) FROM {0}".format(self.name)
    c = self.database.execute_stmt(stmt)
    self.size = c.fetchone()[0]
    return self.size

  def insert(self, cols, data):
    raise NotImplementedError("Implement s[ecific insert() method")

  def insert_data(self, cols, data, condition=None):
    stmt = """INSERT INTO {1} ({2}) VALUES ({3})""".format(condition, self.name, ','.join(x for x in cols), ','.join("?" for x in cols))
    if condition:
      stmt = """INSERT OR {0} INTO {1} ({2}) VALUES ({3})""".format(condition, self.name, ','.join(x for x in cols), ','.join("?" for x in cols))
    c = self.database.conn.cursor()
    c.executemany(stmt, [x for x in data])
    self.database.conn.commit()

  def get_rows(self):
    c = self.database.conn.cursor()
    return c.execute("SELECT * FROM {}".format(self.name))

  def select(self, select, condition=None, values=None):
    if not condition:
      return self.execute_stmt("{0} FROM {1}".format(select, self.name), values)
    else:
      return self.execute_stmt("{0} FROM {1} WHERE {2}".format(select, self.name, condition), values)

  def execute_stmt(self, stmt, values=None):
    c = self.database.conn.cursor()
    if not values:
      return c.execute(stmt.format(self.name))
    return c.execute(stmt.format(self.name), values)

  def create_index(self, idx_name, cols):
    self.execute_stmt("""CREATE INDEX IF NOT EXISTS {0} ON {1} ({2})""".format(idx_name,
                                                                               self.name,
                                                                               ','.join(x for x in cols)))
    self.idx[idx_name] = 0

  def drop_index(self, idx_name):
    self.execute_stmt("""DROP INDEX IF EXISTS {0}""".format(idx_name))
