#-------------------------------------------------------------------------------
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2019 The University of Sydney
#  \description
#-------------------------------------------------------------------------------

#import enum
from . import mbdb_clerk

class MongodbBioCollection:

  def __init__(self, db, namespace_root, write=False):
    self.db = db
    self.namespace_root = namespace_root
    self.scopes = {}
    self.clerk = mbdb_clerk.MongodbBioClerk()
    self.write = write
    self.size = 0

  def collect_entry(self, entry):
    scope = self.db.namespace.resolve(self.namespace_root, entry)
    self.scopes[scope.id] = scope
    self.size += 1
