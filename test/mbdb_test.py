#!/usr/bin/env python3
#-------------------------------------------------------------------------------
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2019 The University of Sydney
#  \description
#-------------------------------------------------------------------------------

import time
import os
import sys
import random
import string
import uuid
import hashlib

sys.path.insert(1, os.path.join(sys.path[0], '../src/'))
from mongobiodb import mbdb_collection
from mongobiodb import mbdb_entry
from mongobiodb import mbdb_driver

random.seed(8888)
alphabet = 'ACTGN'
dbs = ['dba', 'dbb', 'dbc']

class Sequence(mbdb_entry.MongodbBioCollectionEntry):

  def __init__(self, namespace, sequence):
    super().__init__(namespace, uid=hashlib.sha1(sequence.encode()).hexdigest())
    self.sequence = sequence
    self.name = ''.join(random.choice(string.ascii_lowercase) for i in range(10))
    self.dbuid = ''.join(random.choice(string.ascii_lowercase) for i in range(5))
    self.db = random.choice(dbs)

  def write(self):
    return ">{}\n{}\n".format(self.name, self.sequence)

  def get_filename(self):
    return self.uid + '.seq'

  def update_command(self):
    return self.Update(docfilter={'_id' : self.uid},
                       update={'$addToSet' : {'names' : self.name, 'dbs' : {self.db : self.dbuid}}},
                       upsert=True)

def main():
  db = mbdb_driver.MongodbBioDatabaseDriver('mbdbtesting')
  seqs = [['protein', 'predicted'], ['protein', 'predicted'],['protein', 'tested', 'new', 'yes', 'no', 'unknown'],
          ['protein', 'tested', 'old'],['protein', 'protein'], ['tested', 'protein'], ['nuc', 'hyp', 'virus']]

  col = mbdb_collection.MongodbBioCollection(db, 'sequence', write=True)
  for i in seqs:
    seq = ''.join(random.choice(alphabet) for i in range(random.randint(20,30)))
    s = Sequence(i, seq)
    col.collect_entry(s)
    print(s.uid, s.name, s.namespace, s.sequence, sep='\t')
  col.collect_entry(Sequence(['entry'], seq))
  print("Collected {} entries".format(col.size))
  db.add_collection(col)

  return 0

if __name__ == '__main__':
  main()
