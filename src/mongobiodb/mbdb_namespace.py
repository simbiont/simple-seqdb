#-------------------------------------------------------------------------------
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2019 The University of Sydney
#  \description
#-------------------------------------------------------------------------------

import os
import base64

class MongodbBioNamespace:

  class Scope:

    def __init__(self, name, scope_id=None, parent=None):
      self.name = name
      self.children = {}
      self.parent = parent
      self.id = self.make_id() if not scope_id else scope_id
      self.data = []

    def make_id(self):
      return 'scope_' + base64.urlsafe_b64encode(os.urandom(9)).decode()

    def get_children_ids(self):
      return [self.children[x].id for x in self.children]

    def get_parent_id(self):
      if not self.parent:
        return None
      return self.parent.id

    def get_namespace(self):
      namespace = []
      scope = self
      while scope.parent:
        namespace.insert(0, scope)
        scope = scope.parent
      namespace.insert(0, scope)
      return namespace

  def __init__(self):
    self.namespaces = {}

  def resolve(self, collection_root, entry):
    if collection_root not in self.namespaces:
      self.namespaces[collection_root] = self.Scope(collection_root)
    scope = self.namespaces[collection_root]
    for i in entry.namespace:
      if i not in scope.children:
        scope.children[i] = self.Scope(i, parent=scope)
      scope = scope.children[i]
    scope.data.append(entry)
    return scope

  def get_namespaces(self):
    namespaces = {}
    for i in self.namespaces:
      self.walk_namespace(self.namespaces[i], namespaces)
    return namespaces

  def walk_namespace(self, scope, namespaces):
    namespaces.update({scope.name:{'children':{}, 'id':scope.id}})
    if not scope.children:
      namespaces[scope.name]['children'] = None
    for i in scope.children:
      if i not in namespaces[scope.name]['children']:
        namespaces[scope.name]['children'].update({i:{'children':{}, 'id':scope.id}})
      self.walk_namespace(scope.children[i], namespaces[scope.name]['children'])

  def load_namespace(self, namespace_doc):
    for i in namespace_doc:
      if i not in self.namespaces:
        self.namespaces[i] = self.Scope(i, scope_id=namespace_doc[i]['id'])
      self.parse_namespace(self.namespaces[i], namespace_doc)

  def parse_namespace(self, scope, source):
    if source[scope.name]['children']:
      for i in source[scope.name]['children']:
        if i not in scope.children:
          scope.children.update({i:self.Scope(i, scope_id=source[scope.name]['children'][i]['id'],
                                              parent=scope)})
        self.parse_namespace(scope.children[i], source[scope.name]['children'])
