#-------------------------------------------------------------------------------
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2019 The University of Sydney
#  \description
#-------------------------------------------------------------------------------

import os

class MongodbBioClerk:

  @staticmethod
  def write_data(path, data):
    fh = open(path, 'w')
    fh.write(data.write())
    fh.close()

  def __init__(self, rootdir=os.getcwd()):
    self.rootdir = rootdir

  def store(self, data, datadir):
    self.make_datadir(datadir)
    if not os.path.isfile(self.assemble_datapath(datadir, data)):
      MongodbBioClerk.write_data(self.assemble_datapath(datadir, data), data)

  def assemble_datapath(self, datadir, data):
    return os.path.join(datadir, data.get_filename())

  def make_datadir(self, datadir):
    if not os.path.exists(datadir):
      os.makedirs(datadir)
