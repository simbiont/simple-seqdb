#-------------------------------------------------------------------------------
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2019 The University of Sydney
#  \description
#-------------------------------------------------------------------------------

class MongodbBioCollectionEntry:

  class Update:

    def __init__(self, docfilter, update, upsert=False, collation=None,
                 array_filters=None):
      self.filter = docfilter
      self.update = update
      self.upsert = upsert
      self.collation = collation
      self.array_filters = array_filters

  def __init__(self, namespace, uid=None):
    self.namespace = namespace
    self.uid = uid

  def write(self):
    raise NotImplementedError

  def get_filename(self):
    raise NotImplementedError

  def update_command(self):
    raise NotImplementedError
