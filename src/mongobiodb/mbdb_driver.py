#-------------------------------------------------------------------------------
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2019 The University of Sydney
#  \description
#-------------------------------------------------------------------------------

import os
import sys
import pymongo

from . import mbdb_namespace
from . import mbdb_clerk

class MongodbBioDatabaseDriver:

  class Index:

    def __init__(self, collection):
      self.collection = collection
      self.document = 'index'
      self.namespaces = 'namespaces'

  def __init__(self, name, host='localhost', port=27017, index='mbdb_index'):
    self.name = name
    self.port = port
    self.host = host
    self.index = self.Index(index)
    self.client = pymongo.MongoClient("mongodb://{0}:{1}/{2}".format(host, port, name))
    self.database = pymongo.database.Database(self.client, self.name)
    self.namespace = mbdb_namespace.MongodbBioNamespace()
    self.datadir = os.path.join('/tmp', 'mbdb-datadir')
    self.collections = {}
    self.init_db()

  def init_db(self):
    if not self.haveIdx():
      self.create_index_document()
    self.load_index()

  def get_collection(self, name, index=None, options=None):
    if name not in self.collections:
      if options:
        self.collections[name] = pymongo.collection.Collection(self.database, name, options)
      else:
        self.collections[name] = pymongo.collection.Collection(self.database, name)
      if index:
        self.collections[name].create_index(index)
      return self.collections[name]
    return self.collections[name]

  def update_data(self, db_data_collection, scope, write):
    updates = []
    for i in scope.data:
      if write:
        mbdb_clerk.MongodbBioClerk().store(i, self.datadir)
      updates.append(pymongo.operations.UpdateOne(filter=i.update_command().filter,
                                                  update=i.update_command().update,
                                                  upsert=i.update_command().upsert,
                                                  collation=i.update_command().collation,
                                                  array_filters=i.update_command().array_filters))
    refs = []
    for i in db_data_collection.bulk_write(updates).upserted_ids.values():
      refs.append(pymongo.operations.InsertOne({'ref' : db_data_collection.name, 'id' : i,
                                                'db':db_data_collection.database.name}))
    if refs:
      self.get_collection(scope.id).bulk_write(refs)

  def update_namespace_collections(self, scope, updates):
    for i in scope.get_namespace():
      updates.append(pymongo.operations.UpdateOne(filter={'_id':i.id},
                                                  update={'$setOnInsert':{'parent':i.get_parent_id(),
                                                                          'name':i.name},
                                                          '$addToSet':{'children':{
                                                                       '$each':i.get_children_ids()}}},
                                                  upsert=True))

  def add_collection(self, collection):
    db_data_collection = self.get_collection(collection.namespace_root)
    idx_updates = []
    for i in collection.scopes:
      self.update_data(db_data_collection, collection.scopes[i], collection.write)
      self.update_namespace_collections(collection.scopes[i], idx_updates)
    self.get_collection(self.index.collection).bulk_write(idx_updates)
    self.update_index(self.namespace.get_namespaces())

  def update_index(self, new_idx):
    self.get_collection(self.index.collection).update_one({'_id' : self.index.document},
                                                          {'$set' : {self.index.namespaces : new_idx}})

  def create_index_document(self):
    c = self.get_collection(self.index.collection, options={"_id": self.index.namespaces})
    c.insert_one({'_id' : self.index.document, self.index.namespaces : {}})
    print("Created empty index document '{}' in collection '{}'".format(self.index.document,
                                                                        self.index.collection),
          file=sys.stderr)

  def haveIdx(self):
    c = self.get_collection(self.index.collection)
    if c.count() == 0:
      print("No mbdb index document '{}'".format(self.index.document), file=sys.stderr)
      return False

    if c.find({'_id' : self.index.document}, {'_id' : 1}, limit=1).count() == 0:
      print("Empty mbdb  index document '{}' in '{}'".format(self.index.document,
                                                             self.index.collection),
            file=sys.stderr)
      return False
    print("Found mbdb index document '{}' in '{}'".format(self.index.document,
                                                          self.index.collection),
          file=sys.stderr)
    return True

  def load_index(self):
    idx = self.get_collection(self.index.collection).find_one({'_id' : self.index.document},
                                                              {'_id': 0, self.index.namespaces : 1},
                                                              limit=1)
    self.namespace.load_namespace(idx[self.index.namespaces])
    print("Loaded mbdb index from  document '{}' in collection '{}'".format(self.index.document,
                                                                            self.index.collection),
          file=sys.stderr)
