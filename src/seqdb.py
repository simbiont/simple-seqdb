#-------------------------------------------------------------------------------
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \description
#-------------------------------------------------------------------------------

import sqlite3

class SeqdataDatabase:

  tables = {}

  def __init__(self, path=':memory:'):
    self.path = path
    self.conn = None
    self.connect(path)

  def register_table(self, table):
    SeqdataDatabase.tables[table.name] = table

  def get_table(self, name):
    return SeqdataDatabase.tables.get(name)

  def execute_stmt(self, stmt, values=None):
    c = self.conn.cursor()
    if values == None:
      return c.execute(stmt)
    return c.execute(stmt, values)

  def connect(self, path):
    self.conn = sqlite3.connect(path)
    self.conn.row_factory = sqlite3.Row
    self.execute_stmt("PRAGMA foreign_keys=ON")

  def scan_database_tables(self):
    tables = {}
    for i in self.execute_stmt("""SELECT name FROM sqlite_master WHERE type='table'"""):
      tables[i[0]] = 0
    return tables
