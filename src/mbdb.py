#!/usr/bin/env python3
#-------------------------------------------------------------------------------
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2019 The University of Sydney
#  \description
#-------------------------------------------------------------------------------

import io
import os
import sys
import argparse

from mongobiodb import mbdb_driver

class MongodbBioDatabase:

  def __init__(self):
    pass

def main():
  ap = argparse.ArgumentParser("Mbdb")
  ap.add_argument('-db', '--database', type=str)
  subaps = ap.add_subparsers(help='Mbdb aspects')

  ns_ap = subaps.add_parser("namespaces")
  ns_ap.add_argument(dest='show', help='Show namespaces', action='store_true')
  args = ap.parse_args()

  mbdb = mbdb_driver.MongodbBioDatabaseDriver(args.database)
  for i in ns_ap._actions:
    print(i.dest)
  return 0

if __name__ == '__main__':
  main()
